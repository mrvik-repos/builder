FROM docker.io/archlinux:base

COPY gpg.conf /etc/skel/.gnupg/gpg.conf

RUN pacman-key --init && pacman-key --populate archlinux && pacman -Syu --noconfirm base base-devel git  # Update packages and add base-devel

RUN useradd -m -G users,wheel builder\
        && echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

COPY prepare_makepkg /usr/local/bin/prepare_makepkg

USER builder
